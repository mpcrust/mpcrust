# mpcrust

This project is dead.

I abandoned Rust for the following reasons:
* The toolchain is tied in to GitHub. For individual projects I can live with the fact they use
  GitHub, but tying repositories to it is not acceptable to me.
* The community uses mainly proprietary channels.
* Rust is notoriously difficult to [bootstrap], each minor version requiring the previous one
  until you get to the OCaml codebase used for bootstrapping, or to the latest version supported by
  the third-party project mrustc. When you "compile" Rust the "recommended way", it first downloads
  a Rust binary.

The straw that broke the camel's back was YouCompleteMe trying to download rustup to download more
binaries.

[bootstrap]: https://en.wikipedia.org/wiki/Bootstrapping_(compilers)
